new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  data: () => ({
    dialog: false,
    dialogDelete: false,
    dataToDelete: {},
    search: '',
    headers: [
      { text: 'Numero', value: 'num_employe' },
      { text: 'Nom', value: 'nom' },
      { text: 'Actions', value: 'actions', sortable: false },
    ],
    desserts: [],
    editedIndex: -1,
    editedItem: {
      name: '',
    },
    defaultItem: {
      name: '',
    },
  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? 'Nouvel employé' : 'Modifer employé';
    },
  },

  watch: {
    dialog(val) {
      val || this.close();
    },
    dialogDelete(val) {
      val || this.closeDelete();
    },
  },

  created() {
    this.initialize();
  },

  methods: {
    async initialize() {
      this.desserts = await axios('/employe/liste').then((res) => res.data);
    },

    async editItem(item) {
      this.editedIndex = this.desserts.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
    },

    deleteItem(item) {
      this.dataToDelete = item.num_employe;
      this.editedIndex = this.desserts.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogDelete = true;
    },

    async deleteItemConfirm() {
      this.desserts.splice(this.editedIndex, 1);
      await axios.delete('/employe/' + this.dataToDelete).then((res) => {
        this.closeDelete();
      });
    },

    close() {
      this.dialog = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },

    closeDelete() {
      this.dialogDelete = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },

    async save() {
      if (this.editedIndex > -1) {
        await axios
          .patch('/employe/' + this.editedItem.num_employe, {
            nom: this.editedItem.name,
          })
          .then((res) => {
            this.initialize();
          });
      } else {
        await axios
          .post('/employe', { nom: this.editedItem.name })
          .then((res) => {
            this.desserts.push(res.data);
          });
      }
      this.close();
    },
  },
});
