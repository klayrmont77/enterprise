new Vue({
  el: "#app",
  vuetify: new Vuetify(),
  data: () => ({
    dialog: false,
    dialogDelete: false,
    dataToDelete: {},
    enterprises: [],
    employes: [],
    search: "",
    headers: [
      { text: "Numero", value: "id" },
      { text: "Employé", value: "employe.nom" },
      { text: "Entreprise", value: "enterprise.design" },
      {
        text: "Taux horaire (Ar)",
        value: "taux_horaire",
      },
      {
        text: "Nombre d'heure (h)",
        value: "nb_heures",
      },
      {
        text: "Salaire (Ar)",
        value: "salaire",
      },

      { text: "Actions", value: "actions", sortable: false },
    ],
    desserts: [],
    editedIndex: -1,
    editedItem: {
      taux_horaire: "",
      nb_heures: "",
      date_embauche: "",
      employeNumEmploye: "",
      enterpriseNumEnterprise: "",
    },
    defaultItem: {
      taux_horaire: "",
      nb_heures: "",
      date_embauche: "",
      employeNumEmploye: "",
      enterpriseNumEnterprise: "",
    },
  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1
        ? "Nouvelle prestation"
        : "Modifer prestation";
    },
  },

  watch: {
    dialog(val) {
      val || this.close();
    },
    dialogDelete(val) {
      val || this.closeDelete();
    },
  },

  created() {
    this.initialize();
  },

  methods: {
    async initialize() {
      this.desserts = await axios("/travail/liste").then((res) => res.data);
      this.enterprises = await axios("/enterprise/liste").then(
        (res) => res.data
      );
      this.employes = await axios("/employe/liste").then((res) => res.data);
    },

    async editItem(item) {
      this.editedIndex = this.desserts.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
    },

    deleteItem(item) {
      this.dataToDelete = item.id;
      this.editedIndex = this.desserts.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogDelete = true;
    },

    async deleteItemConfirm() {
      this.desserts.splice(this.editedIndex, 1);
      await axios.delete("/travail/" + this.dataToDelete).then((res) => {
        this.closeDelete();
      });
    },

    close() {
      this.dialog = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },

    closeDelete() {
      this.dialogDelete = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },

    async save() {
      if (this.editedIndex > -1) {
        await axios
          .patch("/travail/" + this.editedItem.id, {
            design: this.editedItem.name,
          })
          .then((res) => {
            this.initialize();
          });
      } else {
        // console.log(this.editedItem);
        await axios.post("/travail", this.editedItem).then((res) => {
          this.initialize();
        });
      }
      this.close();
    },
  },
});
