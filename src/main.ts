import { NestExpressApplication } from './../node_modules/@nestjs/platform-express/interfaces/nest-express-application.interface.d';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('ejs');
  // app.setGlobalPrefix('api/v1/');
  await app.listen(3000);
}
bootstrap();
