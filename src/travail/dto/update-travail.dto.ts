import { PartialType } from '@nestjs/mapped-types';
import { CreateTravailDto } from './create-travail.dto';

export class UpdateTravailDto extends PartialType(CreateTravailDto) {}
