import { IsDate, IsNotEmpty, IsNumber } from "class-validator";

export class CreateTravailDto {
  @IsNotEmpty()
  @IsNumber()
  taux_horaire: number;
  @IsNotEmpty()
  @IsDate()
  date_embauche: string;
  @IsNotEmpty()
  emplloye: number;
  @IsNotEmpty()
  enterprise: number;
}
