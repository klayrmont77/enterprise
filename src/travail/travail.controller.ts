import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Render,
} from '@nestjs/common';
import { TravailService } from './travail.service';
import { CreateTravailDto } from './dto/create-travail.dto';
import { UpdateTravailDto } from './dto/update-travail.dto';

@Controller('travail')
export class TravailController {
  constructor(private readonly travailService: TravailService) {}

  @Post()
  create(@Body() createTravailDto: CreateTravailDto) {
    return this.travailService.create(createTravailDto);
  }

  @Get()
  @Render('travail/liste')
  async index() {
    return { travail: await this.travailService.findAll() };
  }

  @Get('liste')
  findAll() {
    return this.travailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.travailService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTravailDto: UpdateTravailDto) {
    return this.travailService.update(+id, updateTravailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.travailService.remove(+id);
  }
}
