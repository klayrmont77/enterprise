import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateTravailDto } from "./dto/create-travail.dto";
import { UpdateTravailDto } from "./dto/update-travail.dto";
import { Travail } from "./entities/travail.entity";

@Injectable()
export class TravailService {
  constructor(
    @InjectRepository(Travail) private travailRepository: Repository<Travail>
  ) {}
  create(createTravailDto: CreateTravailDto) {
    return this.travailRepository.save(createTravailDto);
  }

  async findAll(): Promise<Travail[]> {
    return await this.travailRepository.find({
      relations: {
        employe: true,
        enterprise: true,
      },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} travail`;
  }

  update(id: number, updateTravailDto: UpdateTravailDto) {
    return `This action updates a #${id} travail`;
  }

  async remove(id: number) {
    return await this.travailRepository.delete({ id });
  }
}
