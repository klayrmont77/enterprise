import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TravailService } from './travail.service';
import { TravailController } from './travail.controller';
import { Travail } from './entities/travail.entity';

@Module({
  controllers: [TravailController],
  providers: [TravailService],
  imports: [TypeOrmModule.forFeature([Travail])],
})
export class TravailModule {}
