import { Enterprise } from "./../../enterprise/entities/enterprise.entity";
import { Employe } from "./../../employe/entities/employe.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

@Entity()
export class Travail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  taux_horaire: number;

  @Column()
  nb_heures: number;

  @Column({ type: Date })
  date_embauche: string;
  @ManyToOne(() => Employe, { onDelete: "CASCADE" })
  @JoinColumn()
  employe: number;

  @ManyToOne(() => Enterprise, { onDelete: "CASCADE" })
  @JoinColumn()
  enterprise: number;
}
