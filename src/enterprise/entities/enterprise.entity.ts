import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
export class Enterprise {
  @Column({ unique: true })
  design: string;
  @PrimaryGeneratedColumn()
  num_enterprise: number;
}
