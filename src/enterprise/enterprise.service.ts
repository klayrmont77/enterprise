import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEnterpriseDto } from './dto/create-enterprise.dto';
import { UpdateEnterpriseDto } from './dto/update-enterprise.dto';
import { Enterprise } from './entities/enterprise.entity';

@Injectable()
export class EnterpriseService {
  constructor(
    @InjectRepository(Enterprise)
    private enterpriseRepository: Repository<Enterprise>,
  ) {}
  async create(createEnterpriseDto: CreateEnterpriseDto) {
    return await this.enterpriseRepository
      .save(createEnterpriseDto)
      .catch((e) => {
        throw new Error(e);
      });
  }

  async findAll(): Promise<Enterprise[]> {
    return await this.enterpriseRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} enterprise`;
  }

  async update(id: number, updateEnterpriseDto: UpdateEnterpriseDto) {
    return await this.enterpriseRepository.update(
      { num_enterprise: id },
      updateEnterpriseDto,
    );
  }

  async remove(id: number) {
    return await this.enterpriseRepository.delete({ num_enterprise: id });
  }
}
