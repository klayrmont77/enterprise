import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EnterpriseModule } from './enterprise/enterprise.module';
import { EmployeModule } from './employe/employe.module';
import { TravailModule } from './travail/travail.module';

@Module({
  imports: [
    EnterpriseModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'chat',
      autoLoadEntities: true,
      synchronize: true,
    }),
    EmployeModule,
    TravailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
