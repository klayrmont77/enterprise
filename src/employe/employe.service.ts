import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeDto } from './dto/create-employe.dto';
import { UpdateEmployeDto } from './dto/update-employe.dto';
import { Employe } from './entities/employe.entity';

@Injectable()
export class EmployeService {
  constructor(
    @InjectRepository(Employe) private employeRepository: Repository<Employe>,
  ) {}
  async create(createEmployeDto: CreateEmployeDto): Promise<Employe> {
    return await this.employeRepository.save(createEmployeDto);
  }

  async findAll(): Promise<Employe[]> {
    return await this.employeRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} employe`;
  }

  async update(id: number, updateEmployeDto: UpdateEmployeDto) {
    return await this.employeRepository.update(
      { num_employe: id },
      updateEmployeDto,
    );
  }

  async remove(id: number) {
    return await this.employeRepository.delete({ num_employe: id });
  }
}
