import { IsNotEmpty, IsString } from 'class-validator';

export class CreateEmployeDto {
  @IsNotEmpty()
  @IsString()
  nom: string;
}
