import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employe {
  @PrimaryGeneratedColumn()
  num_employe: number;
  @Column()
  nom: string;
}
